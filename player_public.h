// ============================================
// The header file is for module "player"
// This module is provdied in C style for new programmers in C
// Fall 2016 Gatech ECE 2035
//=============================================
/** @file city_landscape_public.h */
#ifndef PLAYER_PUBLIC_H
#define PLAYER_PUBLIC_H

#include "doubly_linked_list.h"

typedef enum {
    PMISSILE_EXPLODED = 0,
    PMISSILE_ACTIVE = 1
} PLAYER_MISSILE_STATUS; // is missile active or deactive?

typedef enum {
    MOVE_UP = 0,
    MOVE_DOWN = 1,
    MOVE_LEFT = 2,
    MOVE_RIGHT = 3,
    MOVE_UPRIGHT = 4,
    MOVE_UPLEFT = 5,
    MOVE_DOWNRIGHT = 6,
    MOVE_DOWNLEFT = 7
} PLAYER_MOVE_TYPE;

typedef enum {
    LEFT = 0,
    MIDDLE = 1,
    RIGHT = 2
} PLAYER_MISSILE_TURRET;

typedef enum {
    ALIVE = 1,
    DESTROYED = 0
} PLAYER_STATUS; // is player alive or destroyed?

typedef struct {
    int x;                   ///< The x-coordinate of missile current position
    int y;                   ///< The y-coordinate of missile current position
    int x_target;
    int y_target;
    int x_origin;
    int y_origin;
    int tick;             
    float rate;             ///< Depends on what turret the missile comes from
    float dx_t;
    float dy_t;
    PLAYER_MISSILE_STATUS status;   ///< The missile status, see MISSILE_STATUS
} PLAYER_MISSILE; // infomration about missile position and status

typedef struct {
    int x; int y;       // x,y-coordinate of player - top left pixel
    int delta;     // delta x,y
    int delta_diag;
    int width; int height;
    PLAYER_STATUS status;
    DLinkedList* playerMissiles;
} PLAYER; // structure for player


PLAYER player_get_info(void); //returns player's info
DLinkedList* player_missile_get_info(void); //returns a linked list of defensive missiles
void player_init(void); // initialize the player's attributes
void player_move(PLAYER_MOVE_TYPE); //moves player delta pixels in one of 8 directions
void player_fire(PLAYER_MISSILE_TURRET); // fire missiles
void draw_X(int x, int y); //draws target X  
void draw_ammo(void); // draws ammo stores in the turrets
void delete_ammo(PLAYER_MISSILE_TURRET); //draws over the ammo below the turret with landscape color
void player_update(void); // updates cursor position
int get_total_num_missiles(void); //gets total number of missiles remaining
int get_left_num_missiles(void);//gets number of missiles remaining in left turret
int get_middle_num_missiles(void);//gets number of missiles remaining in middle turret
int get_right_num_missiles(void);//gets number of missiles remaining  in right turret
void set_left_num_missiles(int); //sets number of missiles remaining in left turret
void set_middle_num_missiles(int);//sets number of missiles remaining in middle turret
void set_right_num_missiles(int);//sets number of missiles remaining  in right turret
int size_of_player_missile_list(void); //returns the size of the player missile list

void player_missile_update(void); // updates the drawing of missiles on screen
void player_draw(int color); // updates the cursor
void player_missile_draw(PLAYER_MISSILE* missile, int color); // draws defensive missiles

//void player_missile_exploded(int i);
//void player_missile_exploded(PLAYER_MISSILE *playerMissile); //method overload

void player_destroy(void); // destroy the player to end game

#endif //PLAYER_PUBLIC_H