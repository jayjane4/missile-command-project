#ifndef ADVANCEDENEMIES_H
#define ADVANCEDENEMIES_H

/*********************
*functions to control all 3 advanced enemy types in general. 
*********************/


#include "globals.h"
#include "bomber.h"
#include "killer_satellite.h"
#include "smart_bomb.h"


//============external functions===============
//call once at the beginning of each level 
void advanced_enemies_init(void);

//call once every game loop
void update_enemies(void);



//============internal functions===============
//sets the number of bombers/satellites that will appear
void set_NumEnemies(bool);

//gives you the number of advanced enemies left
int get_NumEnemies(void); 

//tells you whether or not there are any advanced enemies left to be launched or if any are active
bool get_Done(void);

//starts an enemy across the screen
void launch_enemy(void);

#endif