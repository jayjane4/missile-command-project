// ============================================
// The file is for module "city landscape"
// This module is provdied in C style for new programmers in C
// 2014 Gatech ECE2035
//=============================================

#include "city_landscape_private.h"
#include "levels.h"
#include "player_public.h"

CITY city_record[MAX_NUM_CITY];
//int building_height[NUM_BUILDING];
//int side_turret_heights[6] = {4,6,7,7,6,4};
//int middle_turret_heights[18] = {3,4,5,6,6,6,6,8,8,8,8,6,6,6,6,5,4,3};

const BYTE cityDesign[MAX_BUILDING_HEIGHT*CITY_WIDTH] = {
    0,0,0,0,0,0,0,0,0,0,1,0,0,
    0,0,1,0,0,0,2,0,0,1,1,0,0,
    0,0,1,1,0,0,2,0,1,1,1,0,0,
    0,1,1,1,0,2,2,2,1,2,1,0,0,
    0,1,2,1,2,2,1,2,2,2,1,1,0,
    0,1,2,2,2,2,1,1,2,2,2,1,0,
    1,2,2,2,2,1,1,1,1,2,2,2,1
};
int cityColors[MAX_BUILDING_HEIGHT*CITY_WIDTH];

// See the comments in city_landscape_public.h
void city_landscape_init(int num_city)
{
    int i;
    //int city_distance = (SIZE_X-CITY_TO_SCREEN_MARGIN*2)/num_city;

    // All interface for user should have error checking
    ASSERT_P(num_city<=MAX_NUM_CITY,ERROR_CITY_NUMBER);

    //initialize the record of cities
    for(i=0; i<MAX_NUM_CITY; i++) {
        if(i<num_city) {
            // See the definition of CITY structure in city_landscape.h
            city_record[i].y = REVERSE_Y(LANDSCAPE_HEIGHT)-1;
            switch(i) {
                case 0  :
                    city_record[i].x = 8;
                    break;
                case 1  :
                    city_record[i].x = 24;
                    break;
                case 2  :
                    city_record[i].x = 40;
                    break;
                case 3  :
                    city_record[i].x = 75;
                    break;
                case 4  :
                    city_record[i].x = 91;
                    break;
                case 5  :
                    city_record[i].x = 107;
                    break;
            }
            //city_record[i].x = i*city_distance + CITY_TO_SCREEN_MARGIN;
            city_record[i].width = CITY_WIDTH;            // the width is fix number
            city_record[i].height = MAX_BUILDING_HEIGHT;  // the height is fix number
            city_record[i].status = EXIST;
        } else {
            city_record[i].status = DESTORIED;
        }
    }

    //initialize the design of the buildings
    //srand(1);

    for(i=0; i<MAX_BUILDING_HEIGHT*CITY_WIDTH; i++) {
        if(cityDesign[i] == 2) {
            cityColors[i] = level.cityColor;
        } else if(cityDesign[i] == 1) {
            cityColors[i] = level.defenseMissileColor;
        } else {
            cityColors[i] = level.backgroundColor;
        }
    }
    /*
    for(i=0; i<NUM_BUILDING; i++) {
        building_height[i] = (rnd() % MAX_BUILDING_HEIGHT*2/3)+MAX_BUILDING_HEIGHT/3;
    }
    */

    //draw city landscape on the screen
    draw_cities();
    draw_landscape();

}

void update_city_landscape(void)
{
    int i;
    for(i=0; i<MAX_BUILDING_HEIGHT*CITY_WIDTH; i++) {
        if(cityDesign[i] == 2) {
            cityColors[i] = level.cityColor;
        } else if(cityDesign[i] == 1) {
            cityColors[i] = level.defenseMissileColor;
        } else {
            cityColors[i] = level.backgroundColor;
        }
    }
}

CITY city_get_info(int index)
{
    // All interface for user should have error checking
    ASSERT_P(index<MAX_NUM_CITY,ERROR_CITY_INDEX_GET_INFO);

    return city_record[index];
}

void city_destory(int index)
{
    int city_x, city_y;

    // error checking. the index must smaller than its max.
    ASSERT_P(index<MAX_NUM_CITY,ERROR_CITY_INDEX_DESTROY);

    // remove the record
    city_record[index].status = DESTORIED;

    // use the background color to cover the city
    city_x = city_record[index].x;
    city_y = city_record[index].y;
    uLCD.filled_rectangle(city_x, city_y, city_x+CITY_WIDTH-1, city_y-(MAX_BUILDING_HEIGHT-1), level.backgroundColor);
}

void draw_cities(void)
{
    int i;
    int city_x, city_y;

    for(i=0; i<MAX_NUM_CITY; i++) {

        // draw each city
        if(city_record[i].status==EXIST) {
            city_x = city_record[i].x;
            city_y = city_record[i].y -(MAX_BUILDING_HEIGHT - 1);
            uLCD.BLIT(city_x, city_y, CITY_WIDTH, MAX_BUILDING_HEIGHT, cityColors);
        }
    }
}


void draw_landscape(void)
{
    //int i,j,k;
    //int side_turret_x, middle_turret_x;
    int turret_y = REVERSE_Y(LANDSCAPE_HEIGHT);
    int height = turret_y - 7;
    uLCD.filled_rectangle(0, SIZE_Y-1, SIZE_X-1, REVERSE_Y(LANDSCAPE_HEIGHT), level.landscapeColor);
    uLCD.filled_rectangle(0, turret_y, 5, height, level.landscapeColor);
    uLCD.filled_rectangle(122, turret_y, 127, height, level.landscapeColor);
    uLCD.filled_rectangle(55, turret_y, 72, height, level.landscapeColor);
    draw_ammo();
}

void rebuild_city(int indexNum)
{
    city_record[indexNum].status = EXIST;
}

int * get_cityColors(void)
{
    return cityColors;
}