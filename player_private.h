// ============================================
// The header file is for module "player
// This module is provdied in C style for new programmers in C
// Fall 2016 Gatech ECE 2035
//=============================================
#ifndef PLAYER_PRIVATE_H
#define PLAYER_PRIVATE_H

#include "mbed.h"
#include "globals.h"
#include "player_public.h"

//==== [private settings] ====
#define PLAYER_INIT_X 63
#define PLAYER_INIT_Y 63
#define PLAYER_DELTA 4 // used in design of player, pixels to move, euclidean distance
#define PLAYER_DELTA_DIAGONAL 3
#define PLAYER_WIDTH 3 
#define PLAYER_HEIGHT 3
#define PLAYER_COLOR 0xFFFFFF //white
#define PLAYER_MISSILE_SPEED 3
#define PLAYER_MISSILE_COLOR 0xFFFF00 //yellow
#define MIDDLE_TURRET_X 63
#define LEFT_TURRET_X 3
#define RIGHT_TURRET_X 124
#define MIDDLE_TURRET_Y 115
#define SIDE_TURRET_Y 115


//==== [private type] ====

void player_draw(int color);
void player_missile_update(void);
void player_missile_draw(PLAYER_MISSILE* missile, int color);

//==== [private function] ====


#endif //PLAYER_PRIVATE_H

