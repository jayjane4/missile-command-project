#include "levels.h"
#include "globals.h"
#include "missile_public.h"
#include "explosions.h"
#include "player_public.h"

LEVEL big_level;

CURRENT_LEVEL level;

int loopTick=0;
int rndIndex;
const int rapidColorSequence[20] = {BLACK_COLOR,WHITE_COLOR,YELLOW_COLOR,RED_COLOR,
                                    CYAN_COLOR,GREEN_COLOR,BLUE_COLOR,BLACK_COLOR,
                                    YELLOW_COLOR,PURPLE_COLOR, RED_COLOR,CYAN_COLOR,
                                    GREEN_COLOR,BLACK_COLOR,WHITE_COLOR,YELLOW_COLOR,
                                    PURPLE_COLOR,RED_COLOR,GREEN_COLOR,BLUE_COLOR
                                   };
const uint16_t randNumberArray[100] = {
    24552, 26327, 38716, 41811, 58545, 52640, 54672, 51886, 3324, 41918,
    25515, 44051, 60628, 55258, 16383, 24423, 50860, 60060, 60345, 53768,
    5790, 37756, 25494, 61391, 18188, 19953, 32842, 51084, 62834, 37143,
    52610, 46920, 34325, 4390, 65289, 59225, 59674, 15888, 26740, 8311,
    41248, 46125, 42713, 19838, 1728, 22535, 54331, 51141, 9862, 15981,
    11914, 37541, 28627, 7546, 31357, 52333, 833, 42861, 23623, 38592,
    19799, 3588, 54589, 31717, 54845, 44132, 9727, 56239, 55506, 22694,
    30285, 34972, 33321, 52035, 12521, 25202, 60580, 5657, 59663, 61189,
    31158, 47237, 41223, 46854, 25229, 15070, 18293, 26779, 38570, 12957,
    58393, 16813, 57801, 53684, 54049, 37660, 57073, 10996, 30530, 23133,
};
//run once at the beginning of session
void big_level_init(void)
{
    big_level.background[0] = BLACK_COLOR; //levels 1-8 are black
    big_level.background[1] = BLACK_COLOR;
    big_level.background[2] = BLACK_COLOR;
    big_level.background[3] = BLACK_COLOR;
    big_level.background[4] = BLUE_COLOR; // levels 9-10
    big_level.background[5] = CYAN_COLOR; // levels 11-12
    big_level.background[6] = PURPLE_COLOR; // levels 13-14
    big_level.background[7] = YELLOW_COLOR; // levels 15-16
    big_level.background[8] = WHITE_COLOR; // levels 17-18
    big_level.background[9] = RED_COLOR; // levels 19-20 then wrap back to 1

    big_level.landscape[0] = YELLOW_COLOR; //levels 1-4 are yellow
    big_level.landscape[1] = YELLOW_COLOR;
    big_level.landscape[2] = BLUE_COLOR;
    big_level.landscape[3] = RED_COLOR;
    big_level.landscape[4] = YELLOW_COLOR; // levels 9-10
    big_level.landscape[5] = YELLOW_COLOR; // levels 11-12
    big_level.landscape[6] = GREEN_COLOR; // levels 13-14
    big_level.landscape[7] = GREEN_COLOR; // levels 15-16
    big_level.landscape[8] = RED_COLOR; // levels 17-18
    big_level.landscape[9] = YELLOW_COLOR; // levels 19-20 then wrap back to 1

    big_level.cities[0] = CYAN_COLOR; //levels 1-4 are cyan
    big_level.cities[1] = CYAN_COLOR;
    big_level.cities[2] = YELLOW_COLOR;
    big_level.cities[3] = YELLOW_COLOR;
    big_level.cities[4] = PURPLE_COLOR; // levels 9-10
    big_level.cities[5] = BLACK_COLOR; // levels 11-12
    big_level.cities[6] = BLACK_COLOR; // levels 13-14
    big_level.cities[7] = WHITE_COLOR; // levels 15-16
    big_level.cities[8] = YELLOW_COLOR; // levels 17-18
    big_level.cities[9] = GREEN_COLOR; // levels 19-20 then wrap back to 1

    big_level.attackMissiles[0] = RED_COLOR; //levels 1-2 are red
    big_level.attackMissiles[1] = GREEN_COLOR;
    big_level.attackMissiles[2] = RED_COLOR;
    big_level.attackMissiles[3] = YELLOW_COLOR;
    big_level.attackMissiles[4] = RED_COLOR; // levels 9-10
    big_level.attackMissiles[5] = RED_COLOR; // levels 11-12
    big_level.attackMissiles[6] = BLACK_COLOR; // levels 13-14
    big_level.attackMissiles[7] = BLACK_COLOR; // levels 15-16
    big_level.attackMissiles[8] = PURPLE_COLOR; // levels 17-18
    big_level.attackMissiles[9] = BLACK_COLOR; // levels 19-20 then wrap back to 1

    big_level.defenseMissiles[0] = BLUE_COLOR; //levels 1-4 are blue
    big_level.defenseMissiles[1] = BLUE_COLOR;
    big_level.defenseMissiles[2] = GREEN_COLOR;
    big_level.defenseMissiles[3] = GREEN_COLOR;
    big_level.defenseMissiles[4] = BLACK_COLOR; // levels 9-10
    big_level.defenseMissiles[5] = BLUE_COLOR; // levels 11-12
    big_level.defenseMissiles[6] = YELLOW_COLOR; // levels 13-14
    big_level.defenseMissiles[7] = RED_COLOR; // levels 15-16
    big_level.defenseMissiles[8] = GREEN_COLOR; // levels 17-18
    big_level.defenseMissiles[9] = BLUE_COLOR; // levels 19-20 then wrap back to 1

}


//run once at beginning of each new game
void level_init(void)
{
    level.levelNum = 0;
    level.backgroundColor = big_level.background[0];
    level.landscapeColor = big_level.landscape[0];
    level.cityColor = big_level.cities[0];
    level.attackMissileColor = big_level.attackMissiles[0];
    level.defenseMissileColor = big_level.defenseMissiles[0];
    level.attackMissileSpeed = 8;
    level.missileNumber = 9;
    level.missileInterval = 100;
    level.scoreMultiplier = 1;
    level.numSmartBombs = 0;
    level.playerColor = level.defenseMissileColor;
    uLCD.background_color(level.backgroundColor);
    uLCD.color(level.attackMissileColor);
    uLCD.textbackground_color(level.backgroundColor);
}

CURRENT_LEVEL get_small_level_struct(void)
{
    return level;
}

LEVEL get_big_level_struct(void)
{
    return big_level;
}

void goto_next_level(void)
{
    DLinkedList* Enemy_Missile_list;
    DLinkedList* Explosion_list;
    DLinkedList* Player_missile_list;
    Player_missile_list = player_missile_get_info();
    Enemy_Missile_list = get_missile_list();
    Explosion_list = get_explosion_list();
    destroyList(Enemy_Missile_list,1);
    destroyList(Explosion_list,1);
    destroyList(Player_missile_list,1);
    int levelMod;
    level.levelNum++;
    levelMod = level.levelNum % 20;
    if(levelMod%2 == 0) {
        level.backgroundColor = big_level.background[levelMod/2];
        level.landscapeColor = big_level.landscape[levelMod/2];
        level.cityColor = big_level.cities[levelMod/2];
        level.attackMissileColor = big_level.attackMissiles[levelMod/2];
        level.defenseMissileColor = big_level.defenseMissiles[levelMod/2];
    } else {
        level.backgroundColor = big_level.background[(levelMod-1)/2];
        level.landscapeColor = big_level.landscape[(levelMod-1)/2];
        level.cityColor = big_level.cities[(levelMod-1)/2];
        level.attackMissileColor = big_level.attackMissiles[(levelMod-1)/2];
        level.defenseMissileColor = big_level.defenseMissiles[(levelMod-1)/2];
    }
    if(level.levelNum <17) {
        if(level.levelNum % 5 == 0)
            level.attackMissileSpeed--;
    }
    if(level.levelNum < 21) {
        if(level.levelNum > 4 ) {
            if (level.levelNum % 4 == 1) {
                level.numSmartBombs++;
            }
        }
        if(level.levelNum % 3 == 1)
            level.missileNumber += 3;
        if(level.levelNum % 3 == 2)
            level.missileInterval -=5;
    }
    if (level.levelNum < 12) {
        if (level.levelNum % 2 == 0) {
            level.scoreMultiplier++;
        }
    }
    level.playerColor = level.defenseMissileColor;
    uLCD.background_color(level.backgroundColor);
    uLCD.color(level.attackMissileColor);
    uLCD.textbackground_color(level.backgroundColor);
}

void seed_rnd(void)
{
    rndIndex = loopTick % 100;
}

int rnd(void)
{
    rndIndex = (rndIndex + 1) % 100;
    return (int)randNumberArray[rndIndex];
}


