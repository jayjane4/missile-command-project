#ifndef SMART_BOMB_H
#define SMART_BOMB_H

#include "globals.h"
#include "missile_public.h"

typedef enum {
    SB_ACTIVE=0,
    SB_DESTROYED=1,
    SB_GONE=2,
} SMART_BOMB_STATUS;

typedef struct {
    int x;
    int y;
    int target_x;
    int target_y;
    SMART_BOMB_STATUS status;
    TARGET_TYPE targetType;
    int targetCity;
    int targetTurret;
} SMARTBOMB ;

//initializes smart bomb colors
void smart_bomb_init(void);

//generates smart bomb
void smart_bomb_launch(void);

//updates smart bomb's position -> including dodging
void update_smart_bomb(void);

//draws smart bomb
void draw_smart_bomb(int);

//returns pointer to the smart bomb
SMARTBOMB * get_smart_bomb(void);

//finds the closest explosion to the smart bomb in order for it to try to dodge
int * find_most_dangerous_explosion(void);

//dodges left
void dodge_left(void);

//dodges up
void dodge_up(void);

//dodges right
void dodge_right(void);

//dodges down
void dodge_down(void);

#endif