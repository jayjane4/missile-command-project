// ============================================
// The header file define the missile module
// Fall 2016 Gatech ECE2035
//=============================================
/** @file missile_public.h */
#ifndef MISSILE_PUBLIC_H
#define MISSILE_PUBLIC_H

#include "doubly_linked_list.h"

typedef enum {
    MISSILE_EXPLODED=0,
    MISSILE_ACTIVE=1,
} MISSILE_STATUS;

typedef enum {
    A_CITY=0,
    A_TURRET=1,
} TARGET_TYPE;

/// The structure to store the information of a missile
typedef struct {
    int x;                   ///< The x-coordinate of missile current position
    int y;                   ///< The y-coordinate of missile current position
    float source_x;           ///< The x-coordinate of the missile's origin
    float source_y;          ///< The y-coordinate of the missile's origin
    //float target_x;           ///< The x-coordinate of the missile's target
    //float target_y;          ///< The y-coordinate of the missile's target
    int tick;                  ///< The missile's internal tick
    MISSILE_STATUS status;   ///< The missile status, see MISSILE_STATUS
    TARGET_TYPE targetType;   ///< Specifies if the missile is targetting a city or a turret
    int targetCity;           ///< Target city
    int targetTurret;           ///< Target turret
    int rate;                 ///< variable dictating missile speed
    float dx,dy;             ///< change in x and change in y per update loop
    bool MIRV;                ///< if true; missle will become MIRV if not destroyed in time
} MISSILE;


/** Call missile_init() only once at the begining of your code */
void missile_init(void);

/** Call missile_init() only once at the begining of your code */
void missile_init(void);

/** This function draw the missiles onto the screen
    Call missile_generator() repeatedly in your game-loop. ex: main()
*/
void missile_generator(void);

/** This function will return a linked-list of all active MISSILE structures.
    This can be used to modify the active missiles. Marking missiles with status
    MISSILE_EXPLODED will cue their erasure from the screen and removal from the
    list at the next missile_generator() call.
*/
DLinkedList* get_missile_list();


//returns pointer to an array of targets for the level
int * get_targets(void);

//returns pointer to ann array of indexes of targeted cities
int * get_LevelCities(void);

//given an x and y coordinate will spawn a MIRV
void launch_MIRV(int, int);


#endif //MISSILE_PUBLIC_H