#ifndef LEVELS_H
#define LEVELS_H

typedef struct {
    int background[10];         ///< array of background colors corresponding to level
    int landscape[10] ;         ///< array of landscape colors corresponding to level
    int cities[10];         ///< array of city colors corresponding to level
    int attackMissiles[10] ;         ///< array of attack missile colors corresponding to level
    int defenseMissiles[10] ;         ///< array of defense missile colors corresponding to level
    
} LEVEL;

//variable structure contains level specific variables
typedef struct {
    int levelNum;                  ///< level ticker
    int backgroundColor; 
    int landscapeColor;
    int cityColor;
    int attackMissileColor;
    int defenseMissileColor;
    int attackMissileSpeed;       ///< varible affecting the rate at which missiles fall
    int missileNumber;            ///< maximum number of missiles spawned in a level
    int missileInterval;          ///< variable affecting the interval in which missiles spawn
    int scoreMultiplier;          ///< level dependent score multiplier
    int playerColor;              ///< indicates color of player
    int numSmartBombs;            ///< number of smart bombs to be spawned in a level
} CURRENT_LEVEL;

//initializes the arrays in the LEVEL struct
void big_level_init(void);

//initializes the variables in the CURRENT_LEVEL struct
void level_init(void);

//return LEVEL struct
LEVEL get_big_level_struct(void);

//return CURRENT_LEVEL struct
CURRENT_LEVEL get_small_level_struct(void);

//advances the proper variables to the next level's specifications
void goto_next_level(void);

//homemade rng seed
void seed_rnd(void);

//homemade rand() function
int rnd(void);

#endif