// ============================================
// The file implement the missile module
// Fall 2016 Gatech ECE2035
//=============================================

#include "missile_private.h"
#include "doubly_linked_list.h"
#include "globals.h"
#include "explosions.h"
#include "levels.h"
#include "city_landscape_public.h"

int missile_tick=0;
int targets[6];
int target;
int levelCities[3];
int maxMissiles;
int totalMissiles;


//Create a DLL for missiles
DLinkedList* missileDLL = NULL;

void missile_init(void)
{
    maxMissiles = level.missileNumber;
    totalMissiles = 0;
    targets[0] = 2;
    targets[1] = 63;
    targets[2] = 125;
    int i;
    int possible_targets[6] = {14,30,46,81,97,113};
    int possible_cities[6] = {-1,-1,-1,-1,-1,-1};
    int pindex = 0;
    CITY myCity;
    int numTargets = 0;
    for(i=0; i<6; i++) {
        myCity = city_get_info(i);
        if(myCity.status == EXIST) {
            possible_cities[pindex] = i;
            pindex++;
            numTargets++;
        }
    }
    pindex = 6;
    int numTargetsFound=0;
    missileDLL = create_dlinkedlist();
    while(numTargetsFound < 3 && possible_cities[0] > -1 ) {
        target = rnd() % numTargets;
        targets[3+numTargetsFound] = possible_targets[possible_cities[target]];
        levelCities[numTargetsFound] = possible_cities[target];
        possible_cities[target] = possible_cities[numTargets-1];
        possible_cities[numTargets-1] = -1;
        possible_targets[target] = possible_targets[pindex-1];
        pindex--;
        numTargets--;
        numTargetsFound++;
    }
    if(numTargetsFound < 3) {
        numTargets = 0;
        pindex = 0;
        for(i=0; i<6; i++) {
            myCity = city_get_info(i);
            if(myCity.status != EXIST) {
                possible_cities[pindex] = i;
                pindex++;
                numTargets++;
            }
        }
        pindex = 6-numTargetsFound;
        while(numTargetsFound < 3) {
            target = rnd() % numTargets;
            targets[3+numTargetsFound] = possible_targets[possible_cities[target]];
            levelCities[numTargetsFound] = possible_cities[target];
            possible_cities[target] = possible_cities[numTargets-1];
            possible_cities[numTargets-1] = -1;
            possible_targets[target] = possible_targets[pindex-1];
            pindex--;
            numTargets--;
            numTargetsFound++;
        }
    }
}

// See the comments in missile_public.h
void missile_generator(void)
{
    missile_tick++;
    // only fire the missile at certain ticks


    if(((missile_tick % level.missileInterval)==0 || missile_tick==0) && (totalMissiles < maxMissiles)) {
        //printf("missile_create()");
        missile_create();
    }

    // update the missiles and draw them
    if(missile_tick >=1000)
        missile_tick = 1;
    missile_update_position();
}

/** This function finds an empty slot of missile record, and active it.
*/
void missile_create(void)
{
    int targetNum;
    int MIRVcounter;
    float target_x;           ///< The x-coordinate of the missile's target
    float target_y;          ///< The y-coordinate of the missile's target
    if(level.levelNum > 10) {
        MIRVcounter = rnd()%4;
    } else {
        MIRVcounter = rnd()%5;
    }
    MISSILE* missle = (MISSILE*)malloc(48);
    missle->rate = level.attackMissileSpeed * 25;
    missle->source_y = 0;
    missle->y = 0;
    //each missile has its own tick
    missle->tick = 0;
    //set a random source for the missile
    missle->source_x = rnd() % SIZE_X;
    //set a random target for the missile
    targetNum = rnd() % 6;
    if(MIRVcounter == 0) {
        missle->MIRV = true;
    } else {
        missle->MIRV = false;
    }
    target_x = targets[targetNum];

    if (targetNum < 3) {
        missle->targetType = A_TURRET;
        missle->targetTurret = targetNum;
    } else {
        missle->targetType = A_CITY;
        missle->targetCity = levelCities[targetNum-3];
    }

    target_y = 117;
    //the missile starts at its source
    missle->x = missle->source_x;

    missle->status = MISSILE_ACTIVE;

    missle->dy = (target_y)/(missle->rate);
    missle->dx = (target_x - missle->source_x)/(missle->rate);

    insertHead(missileDLL, missle);
    totalMissiles++;
}

/** This function update the position of all missiles and draw them
*/
void missile_update_position(void)
{

    MISSILE* newMissile = (MISSILE*)getHead(missileDLL);
    //iterate over all missiles
    while(newMissile) {
        if(newMissile->status == MISSILE_EXPLODED) {
            // clear the missile on the screen

            missile_draw(newMissile, level.backgroundColor);
            explode(newMissile->x,newMissile->y);
            // Remove it from the list
            newMissile = (MISSILE*)deleteForward(missileDLL, 1);
        } else {
            //cover the last missile location
            missile_draw(newMissile, level.attackMissileColor);

            // update missile position
            newMissile->y = (int)(newMissile->source_y + newMissile->dy*(newMissile->tick%(newMissile->rate)));
            newMissile->x = (int)(newMissile->source_x + newMissile->dx*(newMissile->tick%(newMissile->rate)));
            // draw missile
            missile_draw(newMissile, 1);
            //update missile's internal tick
            newMissile->tick++;
            /*
            if (newMissile->tick == 1) {
                uLCD.pixel(newMissile->source_x,newMissile->source_y,level.backgroundColor);
            }
            */
            if((newMissile->y >60) && newMissile->MIRV) {
                launch_MIRV(newMissile->x,newMissile->y);
                missile_draw(newMissile, level.backgroundColor);
                newMissile = (MISSILE*)deleteForward(missileDLL, 1);
            }
            // Advance the loop
            newMissile = (MISSILE*)getNext(missileDLL);
        }
    }
}

//MIRVS come from missiles that have already been launched. at a certain height they split
//into more missiles and go after new targets. the original target may not be included.
//ranges from 2-4 missiles
void launch_MIRV(int init_x, int init_y)
{
    if (totalMissiles <= maxMissiles) {
        int numSplits;
        //int numSplits = (rnd() % 3) +2; //number 2,3, or 4
        if (totalMissiles < maxMissiles) {
            numSplits = 2;
            totalMissiles++;
        } else {
            numSplits = 1;
        }
        int i;
        float target_x;           ///< The x-coordinate of the missile's target
        float target_y;          ///< The y-coordinate of the missile's target
        //make numSplit number of missiles
        for (i=0; i<numSplits; i++) {
            int targetNum;
            MISSILE* missle = (MISSILE*)malloc(48);
            missle->rate = level.attackMissileSpeed * 25;
            missle->source_y = init_y;
            missle->MIRV = false;
            missle->y = missle->source_y;
            //each missile has its own tick
            missle->tick = 0;
            //set a random source for the missile
            missle->source_x = init_x;
            //set a random target for the missile
            targetNum = rnd() % 6;
            target_x = targets[targetNum];

            if (targetNum < 3) {
                missle->targetType = A_TURRET;
                missle->targetTurret = targetNum;
            } else {
                missle->targetType = A_CITY;
                missle->targetCity = levelCities[targetNum-3];
            }

            target_y = 117;
            //the missile starts at its source
            missle->x = missle->source_x;

            missle->status = MISSILE_ACTIVE;

            missle->dy = (target_y - missle->source_y)/(missle->rate);
            missle->dx = (target_x - missle->source_x)/(missle->rate);

            insertHead(missileDLL, missle);
        }
    }
}


/*
// set missile speed (default speed is 4)
void set_missile_speed(int speed)
{
    ASSERT_P(speed>=1 && speed<=8,ERROR_MISSILE_SPEED);
    if(speed>=1 && speed<=8) {
        MISSILE_SPEED = speed;
    }
}

// set missile interval (default interval is 10)
void set_missile_interval(int interval)
{
    ASSERT_P(interval>=1 && interval<=100,ERROR_MISSILE_INTERVAL);
    if(interval>=1 && interval<=100) {
        MISSILE_INTERVAL = interval;
    }
}
*/
// See comments in missile_public.h
DLinkedList* get_missile_list()
{
    return missileDLL;
}

/** This function draw a missile.
    @param missile The missile to be drawn
    @param color The color of the missile
*/
void missile_draw(MISSILE* missile, int color)
{
    if (color == level.attackMissileColor) {
        uLCD.pixel(missile->x,missile->y,color);
    } else if (color == level.backgroundColor) {

        uLCD.line(missile->x-1, missile->y,missile->source_x-1, missile->source_y, color);
        uLCD.line(missile->x+1, missile->y,missile->source_x+1, missile->source_y, color);
        uLCD.line(missile->x, missile->y,missile->source_x, missile->source_y, color);

    } else {
        uLCD.pixel(missile->x,missile->y,rapidColorSequence[loopTick%20]);
    }
    /*
    uLCD.line(missile->x, missile->y,missile->source_x, missile->source_y, color);
    if (color != level.backgroundColor) {
        uLCD.pixel(missile->x,missile->y,rapidColorSequence[loopTick%20]);
    }
    */
}


int * get_targets(void)
{
    return targets;
}
int * get_LevelCities(void)
{
    return levelCities;
}
/*
int get_missile_speed(void)
{
    return MISSILE_SPEED;
}
int get_missile_interval(void)
{
    return MISSILE_INTERVAL;
}
*/