#include "player_private.h"
#include "explosions.h"
#include "levels.h"

PLAYER player; // structure of player
int NumMissileLeft;
int NumMissileRight;
int NumMissileMiddle;

PLAYER player_get_info(void)  // getter for user to acquire info without accessing structure
{
    return player;
}

DLinkedList* player_missile_get_info(void)
{
    return player.playerMissiles;
}

// initialize the player's position, missile status, draw player,
void player_init(void)
{
    //explosion_init();
    player.x = PLAYER_INIT_X;
    player.y = PLAYER_INIT_Y;
    player.status = ALIVE;
    player.playerMissiles = create_dlinkedlist();
    player.delta = PLAYER_DELTA;
    player.delta_diag = PLAYER_DELTA_DIAGONAL;
    player.width = PLAYER_WIDTH;
    player.height = PLAYER_HEIGHT;
    player_draw(level.playerColor);
    NumMissileLeft = 10;
    NumMissileRight = 10;
    NumMissileMiddle = 10;
}

//move player cursor
void player_move(PLAYER_MOVE_TYPE Move_Type)
{
    switch(Move_Type) {
        case MOVE_UP:
            if(player.y-player.delta >= 0) {
                player_draw(level.backgroundColor);
                player.y-=player.delta;
                player_draw(level.playerColor);
            }
            break;
        case MOVE_DOWN:
            if(player.y+player.delta <= 105) {
                player_draw(level.backgroundColor);
                player.y+=player.delta;
                player_draw(level.playerColor);
            }
            break;
        case MOVE_LEFT:
            if(player.x-player.delta >= 0) {
                player_draw(level.backgroundColor);
                player.x-=player.delta;
                player_draw(level.playerColor);
            }
            break;
        case MOVE_RIGHT:
            if(player.x+player.delta <= 127) {
                player_draw(level.backgroundColor);
                player.x+=player.delta;
                player_draw(level.playerColor);
            }
            break;
        case MOVE_UPRIGHT:
            if((player.x+player.delta <= 127) && (player.y-player.delta >= 0)) {
                player_draw(level.backgroundColor);
                player.x+=player.delta_diag;
                player.y-=player.delta_diag;
                player_draw(level.playerColor);
            }
            break;
        case MOVE_UPLEFT:
            if((player.x-player.delta >= 0) && (player.y-player.delta >= 0)) {
                player_draw(level.backgroundColor);
                player.x-=player.delta_diag;
                player.y-=player.delta_diag;
                player_draw(level.playerColor);
            }
            break;
        case MOVE_DOWNLEFT:
            if((player.x-player.delta >= 0) && (player.y+player.delta <= 105)) {
                player_draw(level.backgroundColor);
                player.x-=player.delta_diag;
                player.y+=player.delta_diag;
                player_draw(level.playerColor);
            }
            break;
        case MOVE_DOWNRIGHT:
            if((player.x+player.delta <= 127) && (player.y+player.delta <= 105)) {
                player_draw(level.backgroundColor);
                player.x+=player.delta_diag;
                player.y+=player.delta_diag;
                player_draw(level.playerColor);
            }
            break;
    }
}

// move player PLAYER_DELTA pixels to the left
void player_moveLeft(void)
{
    if (player.x-player.delta >= 0) {
        player_draw(level.backgroundColor);
        player.x-=player.delta;
        player_draw(level.playerColor);
    }
}

// move player PLAYER_DELTA pixels to the right
void player_moveRight(void)
{
    if (player.x+player.delta <= 117) {
        player_draw(level.backgroundColor);
        player.x+=player.delta;
        player_draw(level.playerColor);
    }
}

// generate an active missile to shoot
void player_fire(PLAYER_MISSILE_TURRET Turret)
{
    float dx,dy;
    float speed;

    if (Turret == MIDDLE) {
        if(NumMissileMiddle > 0) {
            PLAYER_MISSILE* playerMissile = (PLAYER_MISSILE*)malloc(48);
            draw_X(player.x,player.y);
            playerMissile->y_origin = MIDDLE_TURRET_Y;
            playerMissile->x_origin = MIDDLE_TURRET_X;
            playerMissile->y = MIDDLE_TURRET_Y;
            playerMissile->x = MIDDLE_TURRET_X;
            speed = 8;
            NumMissileMiddle--;

            playerMissile->y_target = player.y;
            playerMissile->x_target = player.x;
            dx = (playerMissile->x_target - playerMissile->x_origin);
            dy = (playerMissile->y_origin - playerMissile->y_target);
            //printf("dx = %f dy = %f \r\n",dx,dy);
            playerMissile->rate = sqrt(((dx*dx)+(dy*dy))/(speed*speed));
            //printf("rate = %f \r\n",playerMissile->rate);
            playerMissile->dx_t = dx / (playerMissile->rate);
            playerMissile->dy_t = dy / (playerMissile->rate);
            //printf("dx = %f dy = %f \r\n",playerMissile->dx_t,playerMissile->dy_t);
            playerMissile->status = PMISSILE_ACTIVE;
            playerMissile->tick = 0;
            delete_ammo(Turret);
            insertHead(player.playerMissiles, playerMissile);
        }
    } else if (Turret == LEFT) {
        if(NumMissileLeft > 0) {
            PLAYER_MISSILE* playerMissile = (PLAYER_MISSILE*)malloc(48);
            draw_X(player.x,player.y);
            playerMissile->y_origin = SIDE_TURRET_Y;
            playerMissile->x_origin = LEFT_TURRET_X;
            playerMissile->y = SIDE_TURRET_Y;
            playerMissile->x = LEFT_TURRET_X;
            speed = 6;
            NumMissileLeft--;

            playerMissile->y_target = player.y;
            playerMissile->x_target = player.x;
            dx = (playerMissile->x_target - playerMissile->x_origin);
            dy = (playerMissile->y_origin - playerMissile->y_target);
            //printf("dx = %f dy = %f \r\n",dx,dy);
            playerMissile->rate = sqrt(((dx*dx)+(dy*dy))/(speed*speed));
            //printf("rate = %f \r\n",playerMissile->rate);
            playerMissile->dx_t = dx / (playerMissile->rate);
            playerMissile->dy_t = dy / (playerMissile->rate);
            //printf("dx = %f dy = %f \r\n",playerMissile->dx_t,playerMissile->dy_t);
            playerMissile->status = PMISSILE_ACTIVE;
            playerMissile->tick = 0;
            delete_ammo(Turret);
            insertHead(player.playerMissiles, playerMissile);
        }
    } else if (Turret == RIGHT) {
        if(NumMissileRight > 0) {
            PLAYER_MISSILE* playerMissile = (PLAYER_MISSILE*)malloc(48);
            draw_X(player.x,player.y);
            playerMissile->y_origin = SIDE_TURRET_Y;
            playerMissile->x_origin = RIGHT_TURRET_X;
            playerMissile->y = SIDE_TURRET_Y;
            playerMissile->x = RIGHT_TURRET_X;
            speed = 6;
            NumMissileRight--;

            playerMissile->y_target = player.y;
            playerMissile->x_target = player.x;
            dx = (playerMissile->x_target - playerMissile->x_origin);
            dy = (playerMissile->y_origin - playerMissile->y_target);
            //printf("dx = %f dy = %f \r\n",dx,dy);
            playerMissile->rate = sqrt(((dx*dx)+(dy*dy))/(speed*speed));
            //printf("rate = %f \r\n",playerMissile->rate);
            playerMissile->dx_t = dx / (playerMissile->rate);
            playerMissile->dy_t = dy / (playerMissile->rate);
            //printf("dx = %f dy = %f \r\n",playerMissile->dx_t,playerMissile->dy_t);
            playerMissile->status = PMISSILE_ACTIVE;
            playerMissile->tick = 0;
            delete_ammo(Turret);
            insertHead(player.playerMissiles, playerMissile);
        }
    }

}

// draw/updates the line of any active missiles, "erase" deactive missiles
void player_missile_update(void)
{
    //explosion_update_radius();
    PLAYER_MISSILE* playerMissile = (PLAYER_MISSILE*)getHead(player.playerMissiles);
    //int missileNum = 0;
    //delta_x and delta_y account for the slope of the missile
    //double delta_x, delta_y;

    while(playerMissile != NULL) {

        if(playerMissile->status == PMISSILE_EXPLODED) {
            //printf("pmd:exploded\n");
            player_missile_draw(playerMissile,level.backgroundColor);
            playerMissile = (PLAYER_MISSILE*)deleteForward(player.playerMissiles, 1);
        } else {
            player_missile_draw(playerMissile,level.backgroundColor);

            //update posistion
            playerMissile->y = (int)(playerMissile->y_origin - (playerMissile->dy_t)*(playerMissile->tick));
            playerMissile->x = (int)(playerMissile->x_origin + (playerMissile->dx_t)*(playerMissile->tick));

            //determine if missile is drawn again
            if (playerMissile->tick >= playerMissile->rate) {
                playerMissile->status = PMISSILE_EXPLODED;
                //printf("explosion\r\n");
                explode(playerMissile->x_target, playerMissile->y_target);
                playerMissile = (PLAYER_MISSILE*) getNext(player.playerMissiles);
            } else {
                //printf("pmd:normal\n");
                // draw missile
                player_missile_draw(playerMissile,level.defenseMissileColor);
                draw_X(playerMissile->x_target,playerMissile->y_target);
                playerMissile->tick++;
                playerMissile = (PLAYER_MISSILE*) getNext(player.playerMissiles);
            }
        }
    }
}
//}

void player_update(void)
{
    player_draw(level.playerColor);
}

// ==== player_private.h implementation ====
void player_draw(int color)
{
    uLCD.filled_rectangle(player.x-2, player.y, player.x+2, player.y, color);
    uLCD.filled_rectangle(player.x, player.y-2, player.x, player.y+2, color);
}

// destory and "erase" the player off the screen. change status to DESTROYED
void player_destroy()
{
    player_draw(level.backgroundColor);
    player.status = DESTROYED;
}

void player_missile_draw(PLAYER_MISSILE* missile, int color)
{
    int init_x,init_y,current_x,current_y;

    init_x = missile->x_origin;
    init_y = missile->y_origin;
    current_x = missile->x;
    current_y = missile->y;
    uLCD.line(current_x, current_y,init_x, init_y,color);
    if (color == level.backgroundColor) {
        uLCD.pixel(current_x,current_y,color);
    } else {
        uLCD.pixel(current_x,current_y,rapidColorSequence[loopTick%20]);
    }
}

void draw_X(int x, int y)
{
    uLCD.line(x-2,y-2,x+3,y+3,rapidColorSequence[loopTick%20]);
    uLCD.line(x+2,y-2,x-3,y+3,rapidColorSequence[loopTick%20]);
}

//run once at beginning of wave
void draw_ammo(void)
{
    int i;
    for (i=0; i<NumMissileLeft; i++) {
        uLCD.filled_rectangle(i*2 + 1,126,i*2 + 1,124,level.defenseMissileColor);
    }
    for (i=0; i<NumMissileMiddle; i++) {
        uLCD.filled_rectangle(i*2 + 54,126,i*2 + 54,124,level.defenseMissileColor);
    }
    for (i=NumMissileRight; i>0; i--) {
        uLCD.filled_rectangle(127-i*2,126,127-i*2,124,level.defenseMissileColor);
    }
}

void delete_ammo(PLAYER_MISSILE_TURRET Turret)
{
    if(Turret == LEFT) {
        uLCD.filled_rectangle(NumMissileLeft*2 + 1,126,NumMissileLeft*2 + 1,124,level.landscapeColor);
    } else if (Turret == MIDDLE) {
        uLCD.filled_rectangle(NumMissileMiddle*2 + 54,126,NumMissileMiddle*2 + 54,124,level.landscapeColor);
    } else if (Turret == RIGHT) {
        uLCD.filled_rectangle(127-(NumMissileRight+1)*2,126,127-(NumMissileRight+1)*2,124,level.landscapeColor);
    }
}

int get_total_num_missiles(void)
{
    int sum;
    sum = NumMissileLeft + NumMissileRight + NumMissileMiddle;
    return sum;
}


int get_left_num_missiles(void)
{
    return NumMissileLeft;
}

int get_middle_num_missiles(void)
{
    return NumMissileMiddle;
}

int get_right_num_missiles(void)
{
    return NumMissileRight;
}

void set_left_num_missiles(int num)
{
    NumMissileLeft = num;
}

void set_middle_num_missiles(int num)
{
    NumMissileMiddle = num;
}

void set_right_num_missiles(int num)
{
    NumMissileRight = num;
}

int size_of_player_missile_list(void)
{
    return getSize(player.playerMissiles);
}