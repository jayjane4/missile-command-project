#include "advancedEnemies.h"

int direction;
int type;
int NumEnemies;
int nextEnemyTick;
bool Done;

void advanced_enemies_init(void)
{
    nextEnemyTick = 0;
    launch_enemy();
    smart_bomb_init();
}

void update_enemies(void)
{
    if(level.levelNum > 0) {
        if(type == 0) {
            update_bomber_position();
        } else if (type == 1) {
            update_sat_position();
        }
    }
    if(level.levelNum > 4) {
        //printf("update smart bomb \r\n");
        update_smart_bomb();
    }
    nextEnemyTick++;
    if(nextEnemyTick % 500 == 0) {
        nextEnemyTick = 0;
        launch_enemy();
    }


}

void set_NumEnemies(bool nonZero)
{
    if(nonZero) {
        NumEnemies = level.scoreMultiplier;
    } else {
        NumEnemies = 0;
    }
}

int get_NumEnemies(void)
{
    return NumEnemies;
}

void launch_enemy(void)
{
    if(NumEnemies > 0) {
        Done = false;
        direction = rnd() % 2;
        type = rnd() % 2;
        if(type == 0) {
            BOMBER_DIRECTION ThisWay;
            if(direction == 0) {
                ThisWay = GO_RIGHT;
                set_b_direction(ThisWay);
            } else if(direction == 1) {
                ThisWay = GO_LEFT;
                set_b_direction(ThisWay);
            }
            bomber_init();
        } else if(type == 1) {
            SAT_DIRECTION ThisWay;
            if(direction == 0) {
                ThisWay = FLOAT_RIGHT;
                set_s_direction(ThisWay);
            } else if(direction == 1) {
                ThisWay = FLOAT_LEFT;
                set_s_direction(ThisWay);
            }
            sat_init();
        }
        NumEnemies--;
    } else {
        Done = true;
    }
}

bool get_Done(void)
{
    return Done;
}