#ifndef KILLER_SATELLITE_H
#define KILLER_SATELLITE_H

#include "globals.h"

typedef enum {
    S_ACTIVE=0,
    S_OFFSCREEN=1,
    S_DESTROYED=2,
    S_GONE=3
} SAT_STATUS;

typedef enum {
    FLOAT_LEFT=0,
    FLOAT_RIGHT=1
} SAT_DIRECTION;

typedef struct {
    int x;
    int y;
    int launchTick;
    SAT_STATUS status;
    SAT_DIRECTION direction;
} SATELLITE;

//initializes killer satelites
void sat_init(void);

//updates satelites position and launch tick
void update_sat_position(void);

//launches MIRV missile originating from satelite
void sat_launch_missile(void);

//draws satelite with BLIT
void draw_sat(int);

//returns satelite pointer
SATELLITE * get_sat(void);

//set satelite direction
void set_s_direction(SAT_DIRECTION);


#endif