#include "bomber.h"
#include "missile_public.h"
#include "explosions.h"

#define BOMBER_DELTA 1
#define BOMBER_WIDTH 11
#define BOMBER_HEIGHT 9

BOMBER bomber;
int bomberTick = 0;
const BYTE BomberLeft[BOMBER_WIDTH*BOMBER_HEIGHT] = {
    0,0,0,0,0,0,0,1,0,0,0,
    0,0,0,0,0,1,1,1,0,0,1,
    0,0,0,0,1,1,1,0,0,1,1,
    0,1,1,1,1,1,1,1,1,1,1,
    1,1,1,1,1,1,1,1,1,1,0,
    0,0,0,0,1,1,1,0,0,0,0,
    0,0,0,0,0,1,1,1,0,0,0,
    0,0,0,0,0,0,1,1,0,0,0,
    0,0,0,0,0,0,0,1,0,0,0
};
const BYTE BomberRight[BOMBER_WIDTH*BOMBER_HEIGHT] = {
    0,0,0,1,0,0,0,0,0,0,0,
    1,0,0,1,1,1,0,0,0,0,0,
    1,1,0,0,1,1,1,0,0,0,0,
    1,1,1,1,1,1,1,1,1,1,0,
    0,1,1,1,1,1,1,1,1,1,1,
    0,0,0,0,1,1,1,0,0,0,0,
    0,0,0,1,1,1,0,0,0,0,0,
    0,0,0,1,1,0,0,0,0,0,0,
    0,0,0,1,0,0,0,0,0,0,0
};
int colorsLeft[BOMBER_WIDTH*BOMBER_HEIGHT];
int colorsRight[BOMBER_WIDTH*BOMBER_HEIGHT];

void bomber_init(void)
{
    int i;
    if(bomber.direction == GO_LEFT) {
        for(i=0; i<BOMBER_WIDTH*BOMBER_HEIGHT; i++) {
            if(BomberLeft[i] == 1) {
                colorsLeft[i] = level.attackMissileColor;
            } else {
                colorsLeft[i] = level.backgroundColor;
            }
        }
        bomber.x = 121;
    } else if(bomber.direction == GO_RIGHT) {
        for(i=0; i<BOMBER_WIDTH*BOMBER_HEIGHT; i++) {
            if(BomberRight[i] == 1) {
                colorsRight[i] = level.attackMissileColor;
            } else {
                colorsRight[i] = level.backgroundColor;
            }
        }
        bomber.x = 6;
    }
    bomber.y = 40;
    bomber.NumMissiles = (rnd() % 4);
    for(i=0; i<bomber.NumMissiles; i++) {
        bomber.launchTick[i] = (rnd() % 50) + 25;
    }
    if(bomber.NumMissiles < 3) {
        for(i=bomber.NumMissiles; i<4; i++) {
            bomber.launchTick[i] = -1;
        }
    }
    bomber.status = B_ACTIVE;
}

void update_bomber_position(void)
{
    if(bomber.status == B_DESTROYED) {
        explode(bomber.x,bomber.y);
        draw_bomber(level.backgroundColor);
        bomber.status = B_GONE;
    } else {
        if(bomberTick % 3 == 0) {
            if (bomber.status == B_ACTIVE) {
                if(bomber.direction == GO_LEFT) {
                    uLCD.filled_rectangle(bomber.x+5,bomber.y-4,bomber.x+5,bomber.y+4,level.backgroundColor);
                    bomber.x -= BOMBER_DELTA;
                } else if(bomber.direction == GO_RIGHT) {
                    uLCD.filled_rectangle(bomber.x-5,bomber.y-4,bomber.x-5,bomber.y+4,level.backgroundColor);
                    bomber.x += BOMBER_DELTA;
                }
                if(bomber.x >=6 && bomber.x <=121) {
                    draw_bomber(level.attackMissileColor);
                } else {
                    bomber.status = B_OFFSCREEN;
                }
                bomber.launchTick[0]--;
                bomber.launchTick[1]--;
                bomber.launchTick[2]--;
                bomber.launchTick[3]--;

                if(bomber.launchTick[0] == 0) {
                    bomber_launch_missile();
                }
                if(bomber.launchTick[1] == 0) {
                    bomber_launch_missile();
                }
                if(bomber.launchTick[2] == 0) {
                    bomber_launch_missile();
                }
                if(bomber.launchTick[3] == 0) {
                    bomber_launch_missile();
                }
            }
            if (bomber.status == B_OFFSCREEN) {
                draw_bomber(level.backgroundColor);
                bomber.status = B_GONE;
            }

        }
        if(bomberTick > 12)
            bomberTick = 0;
        bomberTick++;
    }
}

void bomber_launch_missile(void)
{
    if (totalMissiles < maxMissiles) {
        DLinkedList* missileDLL = get_missile_list();
        int Missile_speed = level.attackMissileSpeed;
        int targetNum;
        float target_x;           ///< The x-coordinate of the missile's target
        float target_y;          ///< The y-coordinate of the missile's target
        MISSILE* missle = (MISSILE*)malloc(48);
        missle->MIRV = false;
        missle->rate = Missile_speed * 25;
        missle->source_y = bomber.y;
        missle->y = bomber.y;
        //each missile has its own tick
        missle->tick = 0;
        //set a random source for the missile
        missle->source_x = bomber.x;
        //set a random target for the missile
        targetNum = rnd() % 6;
        int * targets = get_targets();
        target_x = *(targets + targetNum);

        if (targetNum < 3) {
            missle->targetType = A_TURRET;
            missle->targetTurret = targetNum;
        } else {
            missle->targetType = A_CITY;
            int * levelCities = get_LevelCities();
            missle->targetCity = *(levelCities + (targetNum - 3));
        }

        target_y = 117;
        //the missile starts at its source
        missle->x = missle->source_x;

        missle->status = MISSILE_ACTIVE;

        missle->dy = (target_y - missle->source_y)/(missle->rate);
        missle->dx = (target_x - missle->source_x)/(missle->rate);

        insertHead(missileDLL, missle);
        bomber.NumMissiles--;
        totalMissiles++;
    }
}

void draw_bomber(int color)
{
    if(bomber.direction == GO_LEFT) {
        if(color == level.backgroundColor) {
            uLCD.filled_rectangle(bomber.x-5,bomber.y-4,bomber.x+5,bomber.y+4,color);
        } else {
            uLCD.BLIT(bomber.x-5,bomber.y-4,BOMBER_WIDTH,BOMBER_HEIGHT,colorsLeft);
        }
    } else if(bomber.direction == GO_RIGHT) {
        if(color == level.backgroundColor) {
            uLCD.filled_rectangle(bomber.x-5,bomber.y-4,bomber.x+5,bomber.y+4,color);
        } else {
            uLCD.BLIT(bomber.x-5,bomber.y-4,BOMBER_WIDTH,BOMBER_HEIGHT,colorsRight);
        }
    }
}

BOMBER * get_bomber(void)
{
    return &bomber;
}

void set_b_direction(BOMBER_DIRECTION thisWay)
{
    bomber.direction = thisWay;
}