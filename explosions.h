#ifndef EXPLOSIONS_H
#define EXPLOSIONS_H


#include "doubly_linked_list.h"

//#define EXPLOSION_COLOR 0x0000FF

typedef enum {
    EXPAND=0,
    RETRACT=1
} EXPLOSION_STATUS;

/// The structure to store the information of an explosion
typedef struct {
    int x;                   ///< The x-coordinate of the center of the explosion
    int y;                   ///< The y-coordinate of the center of the explosion
    int radius;                  ///< The explosion's current radius
    int rsq;
    EXPLOSION_STATUS status;   ///< The explosion's status
} EXPLOSION;

/** Call explosion_init() only once at the begining of your code */
void explosion_init(void);

/** This function will return a linked-list of all active EXPLOSION structures.
    This can be used to look at the active explosions to determine collisions 
*/
DLinkedList* get_explosion_list();

/** This Function creates a new explosion and adds it to the linked list

    @param x the x coordinate of the center of the explosion
    @param y the y coorsinate of the center of the explosion
*/
void explode(int x, int y);

/** This function updates the current radius of all the explosions and deletes those that
    are done with their animations */
void explosion_update_radius(void);

/** This function draws the explosion */
void explosion_draw(EXPLOSION* explosion, int color);


#endif