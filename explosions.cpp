#include "explosions.h"
#include "doubly_linked_list.h"
#include "globals.h"
#include "levels.h"

//Create a DLL for explosions
DLinkedList* explosionDLL = NULL;
bool waitOnce = false;

void explosion_init(void)
{
    explosionDLL = create_dlinkedlist();
}

DLinkedList* get_explosion_list()
{
    return explosionDLL;
}

void explode(int in_x, int in_y)
{
    EXPLOSION* explosion = (EXPLOSION*)malloc(48);
    explosion->x = in_x;
    explosion->y = in_y;
    explosion->status = EXPAND;
    explosion->radius = 0;

    insertHead(explosionDLL,explosion);
}

void explosion_update_radius(void)
{
    EXPLOSION* thisExplosion = (EXPLOSION*)getHead(explosionDLL);

    while(thisExplosion != NULL) {
        //  if(waitOnce) {
//            explosion_draw(thisExplosion, rapidColorSequence[loopTick%20]);
        //    thisExplosion = (EXPLOSION*)getNext(explosionDLL);
        //{ else {
        if(thisExplosion->status == EXPAND) {
            thisExplosion->radius++;
            thisExplosion->rsq = (thisExplosion->radius+2)*(thisExplosion->radius+2);
        } else {
            thisExplosion->radius--;
            thisExplosion->rsq = (thisExplosion->radius+2)*(thisExplosion->radius+2);
        }

        if(thisExplosion->radius == 8) {
            thisExplosion->status = RETRACT;
        }

        if(thisExplosion->radius == 0) {
            explosion_draw(thisExplosion, level.backgroundColor);
            thisExplosion = (EXPLOSION*)deleteForward(explosionDLL, 1);
        } else {

            explosion_draw(thisExplosion, rapidColorSequence[loopTick%20]);
            thisExplosion = (EXPLOSION*)getNext(explosionDLL);
        }

    }

//}
//waitOnce = !waitOnce;
}

void explosion_draw(EXPLOSION* explosion, int color)
{
    uLCD.filled_circle( explosion->x, explosion->y , explosion->radius+1, level.backgroundColor);
    if(explosion->radius != 0) {
        uLCD.filled_circle( explosion->x, explosion->y , explosion->radius, color);
    }
}