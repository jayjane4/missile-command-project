///////////////////////////////////////////////////////////////////////
// Doubly Linked List
//
// This file should contain your implementation of the doubly-linked
// list data structure as described in the project documentation.
//
// Some functions are provided for you - use these as guides for the
// implementation of the remaining functions.
//
// This code can be tested using the testbench module. See
// testbench.h in the source, and LL_testbench_documentation.pdf in
// the project documentation for more information.
//
// GEORGIA INSTITUTE OF TECHNOLOGY, FALL 2016
///////////////////////////////////////////////////////////////////////


#include <stdlib.h>
#include <stdio.h>
#include "doubly_linked_list.h"
#include "globals.h"

/**
 * create_llnode
 *
 * Helper function that creates a node by allocating memory for it on the heap,
 * and initializing its previous and next pointers to NULL and its data pointer to the input
 * data pointer
 *
 * @param data A void pointer to data the user is adding to the doublely linked list.
 * @return A pointer to the linked list node
 */
static LLNode* create_llnode(void* data)
{
    LLNode* newNode = (LLNode*)malloc(48);
    newNode->data = data;
    newNode->previous = NULL;
    newNode->next = NULL;
    return newNode;
}

/**
 * create_dlinkedlist
 *
 * Creates a doublely liked list by allocating memory for it on the heap. Initialize the size to zero,
 * as well as head, current, and tail pointer to NULL
 *
 * @return A pointer to an empty dlinkedlist
 */
DLinkedList* create_dlinkedlist(void)
{
    DLinkedList* newList = (DLinkedList*)malloc(48);
    newList->head = NULL;
    newList->tail = NULL;
    newList->current = NULL;
    newList->size = 0;
    return newList;
}


void insertHead(DLinkedList* dll, void* data)
{
    LLNode* newNode = create_llnode(data);
    if(dll->head == NULL) {
        dll->size++;
        dll->head = newNode;
        dll->tail = newNode;
    } else {
        dll->size++;
        newNode->next = dll->head;
        (dll->head)->previous = newNode;
        dll->head = newNode;
    }
}

void insertTail(DLinkedList* dll, void* data)
{
    //implementation mirrors the given insertHead function
    LLNode* newNode = create_llnode(data);
    if(dll->tail == NULL) {
        dll->size++;
        dll->head = newNode;
        dll->tail = newNode;
    } else {
        dll->size++;
        newNode->previous = dll->tail;
        (dll->tail)->next = newNode;
        dll->tail = newNode;
    }
}

int insertAfter(DLinkedList* dll, void* newData)
{
    LLNode* newNode = create_llnode(newData);
    //if current == NULL do not insert the node and return a 0
    if(dll->current != NULL) {
        //special case if Current == tail
        if(dll->current == dll->tail) {
            //in this case, the new node is the new tail
            newNode->previous = dll->tail;
            (dll->tail)->next = newNode;
            dll->tail = newNode;
        } else {
            //in the generic case link the new node into the list after the current node
            newNode->previous = dll->current;
            newNode->next = (dll->current)->next;
            ((dll->current)->next)->previous = newNode;
            (dll->current)->next = newNode;
        }
        //in either case, increase size and return 1
        dll->size++;
        return 1;
    } else {
        return 0;
    }
}

int insertBefore(DLinkedList* dll, void* newData)
{
    //implementation mirrors insertAfter
    LLNode* newNode = create_llnode(newData);
    if(dll->current != NULL) {
        if(dll->current == dll->head) {
            newNode->next = dll->head;
            (dll->head)->previous = newNode;
            dll->head = newNode;
        } else {
            newNode->next = dll->current;
            newNode->previous = (dll->current)->previous;
            ((dll->current)->previous)->next = newNode;
            (dll->current)->previous = newNode;
        }
        dll->size++;
        return 1;
    } else {
        return 0;
    }
}


void* deleteBackward(DLinkedList* dll, int shouldFree)
{
    //if current==NULL return NULL
    if(dll->current != NULL) {
        LLNode* MyNode = dll->current;
        //this is the generic case
        if(dll->current != dll->head) {
            //special case if current is tail
            if(dll->current == dll->tail) {
                //need to set new tail
                dll->tail = (dll->current)->previous;
            } else {
                //otherwise link the next node to the previous node
                ((dll->current)->next)->previous = (dll->current)->previous;
            }
            // link to the previous node to the next node
            ((dll->current)->previous)->next = (dll->current)->next;
            //update current
            dll->current = (dll->current)->previous;
            //free data if shouldfree is true
            if(shouldFree) {
                free(MyNode->data);
            }
            //free the node
            free(MyNode);
            //decrease the size of the list
            dll->size = (dll->size)-1;
            return (dll->current)->data;
        } else { //special case if current is head
            if((dll->head)->next != NULL) {
                //if head points to another node, assign the new head
                dll->head = (dll->head)->next;
            }
            //update current
            dll->current = NULL;
            //free data if shouldfree is true
            if(shouldFree) {
                free(MyNode->data);
            }
            //free the node and update list size
            free(MyNode);
            dll->size = (dll->size)-1;
            return NULL;
        }
    } else {
        return NULL;
    }
}


void* deleteForward(DLinkedList* dll, int shouldFree)
{
    //implementation mirrors deleteBackwards
    if(dll->current != NULL) {
        LLNode* MyNode = dll->current;
        if(dll->current != dll->tail) {
            if(dll->current == dll->head) {
                dll->head = (dll->current)->next;
            } else {
                ((dll->current)->previous)->next = (dll->current)->next;
            }
            ((dll->current)->next)->previous = (dll->current)->previous;
            dll->current = (dll->current)->next;
            if(shouldFree) {
                free(MyNode->data);
            }
            free(MyNode);
            dll->size = (dll->size)-1;
            return (dll->current)->data;
        } else {
            if (dll->current == dll->head) {
                dll->head = NULL;
            }
            if((dll->tail)->previous != NULL) {
                ((dll->tail)->previous)->next = NULL;
                dll->tail = (dll->tail)->previous;
            } else {
                dll->tail = NULL;
            }
            dll->current = NULL;
            if(shouldFree) {
                free(MyNode->data);
            }
            free(MyNode);
            dll->size = (dll->size)-1;
            return NULL;
        }
    } else {
        return NULL;
    }
}

void destroyList(DLinkedList* dll, int shouldFree)
{
    if(dll->head != NULL) {
        getHead(dll);
        while(deleteForward(dll,shouldFree)) {
        }
    }
    free(dll);
}

void* getHead(DLinkedList* dll)
{
    if(dll->head != NULL) {
        dll->current = dll->head;
        return (dll->head)->data;
    } else {
        return NULL;
    }
}

void* getTail(DLinkedList* dll)
{
    //implementation mirrors the given getHead function
    if(dll->tail != NULL) {
        dll->current = dll->tail;
        return (dll->tail)->data;
    } else {
        return NULL;
    }
}

void* getCurrent(DLinkedList* dll)
{
    //returns current if current is non-null
    if(dll->current != NULL) {
        return (dll->current)->data;
    } else {
        return NULL;
    }
}

void* getNext(DLinkedList* dll)
{
    //gets the data in the node pointed to by current->next and moves the current pointer to that spot
    llnode_t* MyNode;
    if(dll->current != NULL) {
        MyNode = (dll->current)->next;
        if (MyNode != NULL) {
            dll->current = (dll->current)->next;
            return (dll->current)->data;
        } else { //special case where current isn't null but current->next is NULL
            //don't try to extract data from a null pointer
            dll->current = (dll->current)->next;
            return NULL;
        }
    } else {
        return NULL;
    }
}

void* getPrevious(DLinkedList* dll)
{
    //implementation mirrors getNext
    llnode_t* MyNode;
    if((dll->current) != NULL) {
        MyNode = (dll->current)->previous;
        if (MyNode != NULL) {
            dll->current = (dll->current)->previous;
            return (dll->current)->data;
        } else {
            dll->current = (dll->current)->previous;
            return NULL;
        }
    } else {
        return NULL;
    }
}

int getSize(DLinkedList* dll)
{
    //returns size of the list (init is 0 so this works on an empty list)
    return dll->size;
}



