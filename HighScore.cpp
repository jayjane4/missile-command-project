#include "HighScore.h"


DLinkedList* HighScores = NULL;

void readScores(void)
{
    char myName[3];
    int score;
    int ret;
    HIGH_SCORE_PLAYER * topPlayer;
    HighScores = create_dlinkedlist();
    printf("Hello Reading World!\n");
    FILE *fp = fopen("/sd/HighScores/highscores.txt", "r");

    if (fp) {
        ret = fscanf(fp,"%s %d",
                     myName,
                     &score);
        while (ret != EOF) {
            topPlayer = (HIGH_SCORE_PLAYER*)malloc(48);
            //printf("scan\r\n");

            topPlayer->name[0] = myName[0];
            topPlayer->name[1] = myName[1];
            topPlayer->name[2] = myName[2];
            topPlayer->score = score;
            printf("%c%c%c %d\n",topPlayer->name[0],topPlayer->name[1],topPlayer->name[2],topPlayer->score);
            insertTail(HighScores,topPlayer);
            ret = fscanf(fp,"%s %d",
                         myName,
                         &score);
        }
    }
    printf("Goodbye Reading World!\n");
    fclose(fp);
}

DLinkedList* get_highscores(void)
{
    return HighScores;
}

void resetScores(void)
{
    printf("Hello Reset World!\n");

    mkdir("/sd/HighScores", 0777);

    FILE *fp = fopen("/sd/HighScores/highscores.txt", "w");
    if(fp == NULL) {
        error("Could not open file for write\n");
    }
    //fprintf(fp, "High Score Leaderboard: \n\n");
    char name[3];
    name[0] = 'A';
    name[1] = 'A';
    name[2] = 'A';
    int score = 10;
    fprintf(fp,"%c%c%c %d\n", name[0],name[1],name[2],score--);
    fprintf(fp,"%c%c%c %d\n", name[0],name[1],name[2],score--);
    fprintf(fp,"%c%c%c %d\n", name[0],name[1],name[2],score--);
    fprintf(fp,"%c%c%c %d\n", name[0],name[1],name[2],score--);
    fprintf(fp,"%c%c%c %d\n", name[0],name[1],name[2],score--);
    fprintf(fp,"%c%c%c %d\n", name[0],name[1],name[2],score--);
    fprintf(fp,"%c%c%c %d\n", name[0],name[1],name[2],score--);
    fprintf(fp,"%c%c%c %d\n", name[0],name[1],name[2],score--);
    fprintf(fp,"%c%c%c %d\n", name[0],name[1],name[2],score--);
    fprintf(fp,"%c%c%c %d\n", name[0],name[1],name[2],score--);
    fclose(fp);

    printf("Goodbye Reset World!\n");
}


void printScores(void)
{
    int c;
    //mkdir("/sd/HighScores", 0777);
    printf("Hello Printing World!\n");
    FILE *fp = fopen("/sd/HighScores/highscores.txt", "r");
    if (fp) {
        while ((c = getc(fp)) != EOF)
            printf("%c",(char)c);
        fclose(fp);
    }
    printf("Goodbye Printing World!\n");
}

void writeScores(void)
{
    HIGH_SCORE_PLAYER* topPlayer = (HIGH_SCORE_PLAYER*)getHead(HighScores);
    FILE *fp = fopen("/sd/HighScores/highscores.txt", "w");
    if(fp == NULL) {
        error("Could not open file for write\n");
    }
    printf("Hello Writing World!\n");
    while(topPlayer != NULL) {
        printf("%c%c%c %d\n",topPlayer->name[0],topPlayer->name[1],topPlayer->name[2],topPlayer->score);
        fprintf(fp,"%c%c%c %d\n",topPlayer->name[0],topPlayer->name[1],topPlayer->name[2],topPlayer->score);
        topPlayer = (HIGH_SCORE_PLAYER*)getNext(HighScores);
    }

    fclose(fp);
    Destroy_Score_List();

    printf("Goodbye Writing World!\n");
}




void Destroy_Score_List(void)
{
    destroyList(HighScores,1);
}