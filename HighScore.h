#ifndef HIGHSCORE_H
#define HIGHSCORE_H

#include "doubly_linked_list.h"
#include "globals.h"

typedef struct {
    int score;                   ///< The x-coordinate of missile current position
    char name[3];                   ///< The y-coordinate of missile current position
} HIGH_SCORE_PLAYER;


//constructs init highscore board with 0's
//void highScore_init(void);

DLinkedList* get_highscores(void);


// Resets the top 10 scores to 0 USE IF NEW SD CARD
void resetScores(void);

//reads the top 10 highscores and prints it to the terminal
void printScores(void);

//rewrites the file in the SD card with the scores and destroys the score linked list
void writeScores(void);

//reads the top 10 highscores and puts it into the score linked list (also creates linked list)
void readScores(void);

//frees the data allocated by Malloc for the highscore list between matches. 
void Destroy_Score_List(void);

#endif