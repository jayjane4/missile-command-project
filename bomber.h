#ifndef BOMBER_H
#define BOMBER_H

#include "globals.h"

typedef enum {
    B_ACTIVE=0,
    B_OFFSCREEN=1,
    B_DESTROYED=2,
    B_GONE=3
} BOMBER_STATUS;

typedef enum {
    GO_LEFT=0,
    GO_RIGHT=1
} BOMBER_DIRECTION;

typedef struct {
    int x;                  ///< center x of bomber
    int y;                  ///< center y of bomber
    int NumMissiles;        ///< number of missiles that the bomber can drop    
    int launchTick[4];      ///< ticker until missiles are dropped
    BOMBER_STATUS status;   
    BOMBER_DIRECTION direction;
} BOMBER;

//advancedEnemies uses this
void bomber_init(void);

//advancedEnemies uses this
void update_bomber_position(void);

//advancedEnemies uses this
void bomber_launch_missile(void);

//draw bomber BLIT
void draw_bomber(int);

//get bomber
BOMBER * get_bomber(void);

//set bomber direction before init
void set_b_direction(BOMBER_DIRECTION);

#endif