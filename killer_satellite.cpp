#include "killer_satellite.h"
#include "missile_public.h"
#include "explosions.h"

#define SAT_DELTA 1
#define SAT_WIDTH 6
#define SAT_HEIGHT 6


SATELLITE sat;
int satTick = 0;
const BYTE SatLeft[SAT_WIDTH*SAT_HEIGHT] = {
    1,0,1,1,0,1,
    0,1,1,1,1,0,
    1,2,1,2,1,1,
    1,2,1,2,1,1,
    0,1,1,1,1,0,
    1,0,1,1,0,1
};
const BYTE SatRight[SAT_WIDTH*SAT_HEIGHT] = {
    1,0,1,1,0,1,
    0,1,1,1,1,0,
    1,1,2,1,2,1,
    1,1,2,1,2,1,
    0,1,1,1,1,0,
    1,0,1,1,0,1
};
int SatColorsLeft[SAT_WIDTH*SAT_HEIGHT];
int SatColorsRight[SAT_WIDTH*SAT_HEIGHT];


void sat_init(void)
{
    int i;
    if(sat.direction == FLOAT_LEFT) {
        for(i=0; i<SAT_WIDTH*SAT_HEIGHT; i++) {
            if(SatLeft[i] == 1) {
                SatColorsLeft[i] = level.attackMissileColor;
            } else if(SatLeft[i] == 2) {
                SatColorsLeft[i] = level.defenseMissileColor;
            } else {
                SatColorsLeft[i] = level.backgroundColor;
            }
        }
        sat.x = 123;
    } else if(sat.direction == FLOAT_RIGHT) {
        for(i=0; i<SAT_WIDTH*SAT_HEIGHT; i++) {
            if(SatRight[i] == 1) {
                SatColorsRight[i] = level.attackMissileColor;
            } else if(SatRight[i] == 2) {
                SatColorsRight[i] = level.defenseMissileColor;
            } else {
                SatColorsRight[i] = level.backgroundColor;
            }
        }
        sat.x = 4;
    }
    sat.y = 40;
    sat.launchTick = (rnd() % 50) + 25;
    sat.status = S_ACTIVE;
}

void update_sat_position(void)
{
    if(sat.status == S_DESTROYED) {
        explode(sat.x,sat.y);
        draw_sat(level.backgroundColor);
        sat.status = S_GONE;
        sat.launchTick = -1;
    } else {
        if(satTick % 3 == 0) {
            if (sat.status == S_ACTIVE) {
                if(sat.direction == FLOAT_LEFT) {
                    uLCD.filled_rectangle(sat.x+3,sat.y-3,sat.x+3,sat.y+2,level.backgroundColor);
                    sat.x -= SAT_DELTA;
                } else if(sat.direction == FLOAT_RIGHT) {
                    uLCD.filled_rectangle(sat.x-3,sat.y-3,sat.x-3,sat.y+2,level.backgroundColor);
                    sat.x += SAT_DELTA;
                }
                if(sat.x >=4 && sat.x <=123) {
                    draw_sat(level.attackMissileColor);
                } else {
                    sat.status = S_OFFSCREEN;
                }
                sat.launchTick--;
            }
            if (sat.status == S_OFFSCREEN) {
                draw_sat(level.backgroundColor);
                sat.status = S_GONE;
            }

            if(sat.launchTick == 0 && (totalMissiles < maxMissiles)) {
                launch_MIRV(sat.x,sat.y);
            }
        }
        if(satTick > 12)
            satTick = 0;
        satTick++;
    }
}

void draw_sat(int color)
{
    if(sat.direction == FLOAT_LEFT) {
        if(color == level.backgroundColor) {
            uLCD.filled_rectangle(sat.x-2,sat.y-3,sat.x+3,sat.y+2,color);
        } else {
            uLCD.BLIT(sat.x-2,sat.y-3,SAT_WIDTH,SAT_HEIGHT,SatColorsLeft);
        }
    } else if(sat.direction == FLOAT_RIGHT) {
        if(color == level.backgroundColor) {
            uLCD.filled_rectangle(sat.x-3,sat.y-3,sat.x+3,sat.y+2,color);
        } else {
            uLCD.BLIT(sat.x-2,sat.y-3,SAT_WIDTH,SAT_HEIGHT,SatColorsRight);
        }
    }
}

SATELLITE * get_sat(void)
{
    return &sat;
}

void set_s_direction(SAT_DIRECTION ThisWay)
{
    sat.direction = ThisWay;
}

