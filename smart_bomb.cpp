#include "smart_bomb.h"
#include "explosions.h"

#define SMART_BOMB_WIDTH 5
#define SMART_BOMB_DODGE 1
#define SMART_BOMB_DELTA 2

//-----------------DESCRIPTION--------------------
/*
-The smart bomb sees an explosion and tries to move around it.
-It will move up, right, or left to try to dodge an explosion that's in its way
-Technique to kill it = hit it directly or squeeze it between two explosions
-moves slower than missiles do
-search for explosions within a certain range (perhaps a cone? )
-heads toward target unless avoiding something but always tries to dodge in an efficient way
-appears At wave 6 (level.levelNum = 5) in original game
-counts as a missile with the total tally
*/
//------------------------------------------------


SMARTBOMB smartBomb;
int smartBombTick;
int NumBombs;
int updateTick;
int dangerousCoords[2];

const BYTE SmartBombDesign[SMART_BOMB_WIDTH*SMART_BOMB_WIDTH] = {
    0,0,1,0,0,
    0,1,2,1,0,
    1,2,0,2,1,
    0,1,2,1,0,
    0,0,1,0,0
};

int SmartBombColors[SMART_BOMB_WIDTH*SMART_BOMB_WIDTH];

// initializes smart bombs for the level
void smart_bomb_init(void)
{
    smartBombTick = 0;
    updateTick=0;
    NumBombs = level.numSmartBombs;;
    if(NumBombs > 0) {
        int i;
        for(i=0; i<SMART_BOMB_WIDTH*SMART_BOMB_WIDTH; i++) {
            if(SmartBombDesign[i] == 0) {
                SmartBombColors[i] = level.backgroundColor;
            } else if (SmartBombDesign[i] == 1) {
                SmartBombColors[i] = level.attackMissileColor;
            } else if (SmartBombDesign[i] == 2) {
                SmartBombColors[i] = level.cityColor;
            }
        }
    }
    smartBomb.status = SB_GONE;

}

void smart_bomb_launch(void)
{
    //printf("launch smart bomb \r\n");
    smartBomb.x = rnd()% SIZE_X;
    smartBomb.y = 3; //change to 3 if you don't see it draw
    int targetNum;
    targetNum = rnd() % 6;
    int * targets = get_targets();
    smartBomb.target_x = *(targets + targetNum);

    if (targetNum < 3) {
        smartBomb.targetType = A_TURRET;
        smartBomb.targetTurret = targetNum;
    } else {
        smartBomb.targetType = A_CITY;
        int * levelCities = get_LevelCities();
        smartBomb.targetCity = *(levelCities + (targetNum - 3));
    }
    smartBomb.target_y = 117;
    smartBomb.status = SB_ACTIVE;
    NumBombs--;
    totalMissiles++;
}


void update_smart_bomb(void)
{
    if(smartBomb.status == SB_DESTROYED) {
        //printf("branch 1 \r\n");
        draw_smart_bomb(level.backgroundColor);
        explode(smartBomb.x,smartBomb.y);
        smartBomb.status = SB_GONE;
    } else if(smartBomb.status == SB_ACTIVE) {
        //printf("branch 2 \r\n");

        if(updateTick %3 == 0) {
            int * DodgeThis = find_most_dangerous_explosion();
            //no threat nearby if this is true
            draw_smart_bomb(level.backgroundColor);
            if( *(DodgeThis) == (smartBomb.x-8) && *(DodgeThis+1) == (smartBomb.y-8)) {

                int diffx = smartBomb.target_x - smartBomb.x;
                int diffy = smartBomb.target_y - smartBomb.y;
                if(diffx == 0) {
                    smartBomb.y +=4;
                } else {
                    float ratio = ((float)diffy)/((float)diffx);
                    if (ratio > 5 || ratio < -5) {
                        smartBomb.y +=4;
                    } else if (ratio >= 2 && ratio <= 5) {
                        smartBomb.y +=3;
                        smartBomb.x++;
                    } else if (ratio <= -2 && ratio >= -5) {
                        smartBomb.y +=3;
                        smartBomb.x--;
                    } else if (ratio > -2 && ratio < -0.5) {
                        smartBomb.y +=2;
                        smartBomb.x -=2;
                    } else if (ratio < 2 && ratio > 0.5) {
                        smartBomb.y +=2;
                        smartBomb.x +=2;
                    } else if (ratio >= 0 && ratio <= 0.5) {
                        smartBomb.y++;
                        smartBomb.x +=3;
                    } else if (ratio < 0 && ratio >= -0.5) {
                        smartBomb.y++;
                        smartBomb.x -=3;
                    }
                }

            } else {
                int dx = *(DodgeThis) - smartBomb.x;
                int dy = *(DodgeThis+1) - smartBomb.y;
                if(dx == 0 && dy ==0) {
                    //do nothing
                } else if(abs(dy)==abs(dx)) {
                    if(dy<0) {
                        if(dx<0) {
                            dodge_right();
                        } else {
                            dodge_left();
                        }
                    } else {
                        dodge_up();
                    }
                } else if(abs(dy) > abs(dx)) {
                    if(dy<0) {
                        dodge_down();
                    } else {
                        dodge_up();
                    }
                } else if(abs(dy) < abs(dx)) {
                    if(dx<0) {
                        dodge_right();
                    } else {
                        dodge_left();
                    }
                }
            }
            draw_smart_bomb(1);
        }
        if(updateTick > 12)
            updateTick = 0;
        updateTick++;
    } else {
        //printf("branch 3 \r\n");
        smartBombTick++;
        if(smartBombTick % 400 == 0) {
            // printf("branch 3 \r\n");
            smartBombTick = 0;
            if(NumBombs > 0) {
                smart_bomb_launch();
            }
        }
    }

}


void draw_smart_bomb(int color)
{
    if(color == level.backgroundColor) {
        uLCD.filled_rectangle(smartBomb.x-2,smartBomb.y-2,smartBomb.x+2,smartBomb.y+2,color);
    } else {
        uLCD.BLIT(smartBomb.x-2,smartBomb.y-2,SMART_BOMB_WIDTH,SMART_BOMB_WIDTH,SmartBombColors);
    }
}

SMARTBOMB * get_smart_bomb(void)
{
    return &smartBomb;
}

int * find_most_dangerous_explosion(void)
{
    DLinkedList* Explosion_list = get_explosion_list();
    dangerousCoords[0] = smartBomb.x-8;
    dangerousCoords[1] = smartBomb.y-8;
    EXPLOSION * explosion = (EXPLOSION*)getHead(Explosion_list);
    while(explosion != NULL) {
        if((abs(explosion->x - smartBomb.x) <= dangerousCoords[0]) && (abs(explosion->y - smartBomb.y) <= dangerousCoords[1])) {
            dangerousCoords[0] = explosion->x;
            dangerousCoords[1] = explosion->y;
        }
        explosion = (EXPLOSION*)getNext(Explosion_list);
    }
    return dangerousCoords;
}

void dodge_left(void)
{
    smartBomb.x -=2;
}

void dodge_up(void)
{
    smartBomb.y -=2;
}

void dodge_right(void)
{
    smartBomb.x +=2;
}

void dodge_down(void)
{
    smartBomb.y +=2;
}